package uz.pdp.appsecond.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttp;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appsecond.entity.Order;
import uz.pdp.appsecond.payload.BookDTO;
import uz.pdp.appsecond.payload.OrderDTO;
import uz.pdp.appsecond.repository.OrderRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final OkHttpClient client = new OkHttpClient();

    private final OrderRepository orderRepository;


    @GetMapping
    public List<OrderDTO> list() {
        return orderRepository.findAll().stream()
                .map(this::mapOrderDTO)
                .collect(Collectors.toList());
    }


    @PostMapping
    public Order add(@RequestBody Order order) {
        return orderRepository.save(order);
    }

    private OrderDTO mapOrderDTO(Order order) {
        return OrderDTO.builder()
                .id(order.getId())
                .createdAt(order.getCreatedAt())
                .phone(order.getPhone())
                .books(mapBookDTOList(order.getBooks()))
                .build();
    }

    private List<BookDTO> mapBookDTOList(List<String> bookIds) {
        return bookIds.stream().map(this::mapBookDTO).collect(Collectors.toList());
    }

    private BookDTO mapBookDTO(String bookId) {
        Request request = new Request.Builder()
                .url("http://localhost:8080/books/" + bookId)
                .build();

        try (Response response = client.newCall(request).execute()) {
            assert response.body() != null;
            String json = response.body().string();
            System.out.println(json);
            return objectMapper.readValue(json, BookDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
